package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"gopkg.in/russross/blackfriday.v2"
)

func main() {
	port, _ := strconv.Atoi(os.Args[1])
	fmt.Printf("Starting server at Port %d", port)

	http.HandleFunc("/markdown", GenerateMarkdown)
	http.Handle("/", http.FileServer(http.Dir("public")))

	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}

func GenerateMarkdown(rw http.ResponseWriter, r *http.Request) {
	markdown := blackfriday.Run([]byte(r.FormValue("body")), blackfriday.WithNoExtensions())
	rw.Write(markdown)
}
