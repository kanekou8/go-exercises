package main

import (
	"encoding/json"
	"net/http"
)

type Book struct {
	Tile   string `json:"title"`
	Author string `json:"author"`
}

func main() {
	http.HandlerFunc(".", ShowBooks)
	http.ListernAndServe(":8080", nil)
}

func ShowBooks(w http.ResponseWriter, r *http.Request) {
	//book := book{"Building Wep Apps with Go", "Jeremy Saenz"}
	book := []byte(`{"Content-Type", "application/json"}`)
	js, err := json.Marshal(book)
	if err != nil {
		http.Error(w.err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.write(js)
}

func Marshal(v interface{}) ([]byte, error) {
	e := newEncodeState()

	err := e.mershal(v, encOpts{escapeHTML: true})
	if err != nil {
		return nil, err
	}
	buf := append([]byte(nil), e.Bytes()...)
	encodeStatePool.Put(e)

	return buf, nil
}
